package main

import (
	"net/http"

	"github.com/gorilla/mux"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/sirupsen/logrus"

	"gitlab.com/gitlab-org/release-tools/metrics/pkg/handlers"
	"gitlab.com/gitlab-org/release-tools/metrics/pkg/logger"
)

func buildRoutes(log *logrus.Logger, authToken string, metricsHandlers, webhookHandlers []handlers.Pluggable) http.Handler {
	r := mux.NewRouter()
	r.Use(logger.AccessLogMiddleware(log))

	r.Handle("/metrics", promhttp.Handler())

	apiRouter := r.PathPrefix("/api").Subrouter()
	apiRouter.Use(handlers.PrivateTokenMiddleware(authToken))

	log.WithField("number_of_metrics", len(metricsHandlers)).Info("Plugging metrics API handlers")
	for _, h := range metricsHandlers {
		h.PlugRoutes(apiRouter)
	}

	webhooksRouter := r.PathPrefix("/webhooks").Subrouter()

	log.Info("Plugging webhooks API handlers")
	for _, h := range webhookHandlers {
		h.PlugRoutes(webhooksRouter)
	}

	return r
}
