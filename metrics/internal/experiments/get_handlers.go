package experiments

import (
	"gitlab.com/gitlab-org/release-tools/metrics/pkg/handlers"
	"gitlab.com/gitlab-org/release-tools/metrics/pkg/metrics"
)

const (
	subsystem string = "experiments"
)

var pluggables = make([]handlers.Pluggable, 0)

func GetHandlers() ([]handlers.Pluggable, error) {
	counter, err := metrics.NewCounterVec(
		metrics.WithName("total"),
		metrics.WithSubsystem(subsystem),
		metrics.WithHelp("Number of active experiments"),
	)
	if err != nil {
		return nil, err
	}

	for i := 0; i < len(pluggables); i++ {
		counter.Inc()
	}

	return pluggables, nil
}
