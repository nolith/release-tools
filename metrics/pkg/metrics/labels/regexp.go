package labels

import (
	"regexp"
)

var minorVersion = regexp.MustCompile(`^\d+.\d+$`)

func FromRegexp(name string, r *regexp.Regexp) *regularExpression {
	return &regularExpression{
		base:    &base{name: name},
		matcher: r,
	}
}

func MinorVersion(name string) *regularExpression {
	return FromRegexp(name, minorVersion)
}

type regularExpression struct {
	matcher *regexp.Regexp

	*base
}

func (r *regularExpression) Values() []string {
	return nil
}

func (r *regularExpression) CheckValue(value string) bool {
	return r.matcher.MatchString(value)
}
