package pipelines

import (
	"context"
	"fmt"
	"testing"

	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"github.com/xanzy/go-gitlab"
)

func TestFetchPipelinesPaginationAndLimits(t *testing.T) {
	client := newMockPipelinesClient(t)
	obs := observer{
		ctx: context.Background(),
		log: logrus.New(),
		ops: client,
	}

	version := func(idx int) string {
		return fmt.Sprintf("42.%d.202206172020", idx)
	}
	runningPipelines := 2*limit - 1

	success := gitlab.PipelineInfo{
		Ref:    version(runningPipelines + 1),
		Status: string(gitlab.Success),
		Source: "push",
	}

	apiPipelines := make([]*gitlab.PipelineInfo, 0)

	for i := 0; i < runningPipelines; i++ {
		apiPipelines = append(apiPipelines, &gitlab.PipelineInfo{
			Ref:    version(runningPipelines - i),
			Status: string(gitlab.Running),
			Source: "push",
		})
	}
	apiPipelines = append(apiPipelines, &success)

	// an old pipeline that should not be counted
	apiPipelines = append(apiPipelines, &gitlab.PipelineInfo{
		Ref:    version(runningPipelines + 2),
		Status: string(gitlab.Failed),
		Source: "push",
	})

	// first page
	client.On("ListProjectPipelines",
		releaseToolsProject,
		mock.MatchedBy(func(opts *gitlab.ListProjectPipelinesOptions) bool {
			return opts.Status == nil && *opts.Source == "push" &&
				opts.ListOptions.PerPage == limit
		}),
		mock.Anything).Once().Return(apiPipelines[:limit], &gitlab.Response{NextPage: 2}, nil)

	// second page
	client.On("ListProjectPipelines",
		releaseToolsProject,
		mock.MatchedBy(func(opts *gitlab.ListProjectPipelinesOptions) bool {
			return opts.Status == nil && *opts.Source == "push" &&
				opts.ListOptions.Page == 2
		}),
		mock.Anything).Once().Return(apiPipelines[limit:], &gitlab.Response{NextPage: 3}, nil)

	pipelines, err := obs.fetchPipelines()
	assert.NoError(t, err)
	assert.Lenf(t, pipelines, len(apiPipelines)-2, "expected %d pipelines, got %d. pipelines should not contain the last successful pipeline", len(apiPipelines)-2, len(pipelines))
}

func TestFetchPipelinesFiltering(t *testing.T) {
	client := newMockPipelinesClient(t)
	obs := observer{
		ctx: context.Background(),
		log: logrus.New(),
		ops: client,
	}

	apiPipelines := []*gitlab.PipelineInfo{
		// not ok: not a product version
		{
			Ref:    "master",
			Status: string(gitlab.Running),
			Source: "push",
		},
		// ok: a running deployment
		{
			Ref:    "1.3.202201012230",
			Status: string(gitlab.Running),
			Source: "push",
		},
		// limit: the last successful deployment
		{
			Ref:    "1.2.202201012230",
			Status: string(gitlab.Success),
			Source: "push",
		},
		// not ok: past the limit
		{
			Ref:    "1.0.202201012230",
			Status: string(gitlab.Failed),
			Source: "push",
		},
	}

	client.On("ListProjectPipelines",
		releaseToolsProject,
		mock.Anything,
		mock.Anything).Once().Return(apiPipelines, &gitlab.Response{NextPage: 2}, nil)

	pipelines, err := obs.fetchPipelines()
	assert.NoError(t, err)
	assert.Lenf(t, pipelines, 1, "this should only return the running coordinated pipeline")
}
