package handlers

import (
	"net/http"
	"testing"

	"github.com/stretchr/testify/require"

	"gitlab.com/gitlab-org/release-tools/metrics/mocks"
)

func TestReleaseToolsJobWebhookHandlerWithWrongToken(t *testing.T) {
	_, resetJobEventMetricStub := stubJobEventMetrics(t)
	defer resetJobEventMetricStub()

	_, resetJobRetriesMetricStub := stubJobRetriesMetric(t)
	defer resetJobRetriesMetricStub()

	tokenMap := map[string]string{
		ProjectReleaseToolsOps: "test1",
		ProjectDeployer:        "test2",
	}
	handler := NewJobWebhook(tokenMap).(*jobWebhook)

	req, err := webhookRequest(webhookRequestJson(), "/ops/gitlab-org/release/tools", "test2")
	require.NoError(t, err)

	rr := testRequest(req, handler)

	require.Equalf(t, http.StatusUnauthorized, rr.Code, "handler returned wrong status code: got %v want %v",
		rr.Code, http.StatusUnauthorized)
}

func TestDeployerJobWebhookHandlerWithWrongToken(t *testing.T) {
	_, resetJobEventMetricStub := stubJobEventMetrics(t)
	defer resetJobEventMetricStub()

	_, resetJobRetriesMetricStub := stubJobRetriesMetric(t)
	defer resetJobRetriesMetricStub()

	tokenMap := map[string]string{
		ProjectReleaseToolsOps: "test1",
		ProjectDeployer:        "test2",
	}
	handler := NewJobWebhook(tokenMap).(*jobWebhook)

	req, err := webhookRequest(webhookRequestJson(), "/ops/gitlab-com/gl-infra/deployer", "test1")
	require.NoError(t, err)

	rr := testRequest(req, handler)

	require.Equalf(t, http.StatusUnauthorized, rr.Code, "handler returned wrong status code: got %v want %v",
		rr.Code, http.StatusUnauthorized)
}

func TestDeployerJobWebhookHandlerWithoutToken(t *testing.T) {
	_, resetJobEventMetricStub := stubJobEventMetrics(t)
	defer resetJobEventMetricStub()

	_, resetJobRetriesMetricStub := stubJobRetriesMetric(t)
	defer resetJobRetriesMetricStub()

	tokenMap := map[string]string{ProjectReleaseToolsOps: "test1"}
	handler := NewJobWebhook(tokenMap).(*jobWebhook)

	req, err := webhookRequest(webhookRequestJson(), "/ops/gitlab-com/gl-infra/deployer", "")
	require.NoError(t, err)

	rr := testRequest(req, handler)

	require.Equalf(t, http.StatusUnauthorized, rr.Code, "handler returned wrong status code: got %v want %v",
		rr.Code, http.StatusUnauthorized)
}

func TestJobWebhookDeployerPipeline(t *testing.T) {
	testJobWebhookPipelineName(
		t,
		ProjectDeployer,
		"/ops/gitlab-com/gl-infra/deployer",
		"auto-deploy: gprd - 15.9.202301250820-cda2eac9bf1.b2fd4b7b8ae",
	)
}

func TestJobWebhookReleaseToolsPipeline(t *testing.T) {
	testJobWebhookPipelineName(t, ProjectReleaseToolsOps, "/ops/gitlab-org/release/tools", "Coordinator pipeline")
}

func TestJobWebhookQualityStagingCanaryPipeline(t *testing.T) {
	testJobWebhookPipelineName(
		t,
		ProjectQualityStagingCanaryOps,
		"/ops/gitlab-org/quality/staging-canary",
		"Deployment QA pipeline - 15.9.202301301120-a8fdcfa502e.541ff7fe8a6",
	)
}

func TestJobWebhookQualityStagingPipeline(t *testing.T) {
	testJobWebhookPipelineName(
		t,
		ProjectQualityStagingOps,
		"/ops/gitlab-org/quality/staging",
		"Deployment QA pipeline - 15.9.202301301120-a8fdcfa502e.541ff7fe8a6",
	)
}

func TestJobWebhookK8sWorkloadsPipeline(t *testing.T) {
	testJobWebhookPipelineName(
		t,
		ProjectK8sWorkloadsGitlabComOps,
		"/ops/gitlab-com/gl-infra/k8s-workloads/gitlab-com",
		"auto-deploy: gstg-cny - 15-9-202301301120-a8fdcfa502e",
	)
}

func TestJobWebhookCNGPipeline(t *testing.T) {
	testJobWebhookPipelineName(t, ProjectCNGDev, "/dev/gitlab/charts/components/images", "AUTO_DEPLOY_BUILD_PIPELINE")
}

func TestJobWebhookOmnibusPipeline(t *testing.T) {
	testJobWebhookPipelineName(t, ProjectOmnibusDev, "/dev/gitlab/omnibus-gitlab", "AUTO_DEPLOY_BUILD_PIPELINE")
}

func TestJobWebhookNonAutoDeployPipeline(t *testing.T) {
	jobEventMetricsCounter, resetJobEventMetricStub := stubJobEventMetrics(t)
	defer resetJobEventMetricStub()

	jsonData := webhookRequestJson()
	jsonData["commit"].(map[string]interface{})["name"] = "Some other pipeline"

	jobEventMetricsCounter.On("Inc", ProjectReleaseToolsOps, "others").Once()

	assertRequest(t, jsonData, "/ops/gitlab-org/release/tools", ProjectReleaseToolsOps)
}

func TestJobWebhookRecordOnlyCreated(t *testing.T) {
	jobEventMetricsCounter, resetJobEventMetricStub := stubJobEventMetrics(t)
	defer resetJobEventMetricStub()

	_, resetJobRetriesMetricStub := stubJobRetriesMetric(t)
	defer resetJobRetriesMetricStub()

	jobEventMetricsCounter.On("Inc", ProjectDeployer, "auto_deploy").Once()

	// Set build_status to something other than "created"
	jsonData := webhookRequestJson()
	jsonData["build_status"] = "running"

	assertRequest(t, jsonData, "/ops/gitlab-com/gl-infra/deployer", ProjectDeployer)
}

func TestJobWebhookRecordOnlyRetried(t *testing.T) {
	jobEventMetricsCounter, resetJobEventMetricStub := stubJobEventMetrics(t)
	defer resetJobEventMetricStub()

	_, resetJobRetriesMetricStub := stubJobRetriesMetric(t)
	defer resetJobRetriesMetricStub()

	jobEventMetricsCounter.On("Inc", ProjectDeployer, "auto_deploy").Once()

	jsonData := webhookRequestJson()
	jsonData["retries_count"] = 0

	assertRequest(t, jsonData, "/ops/gitlab-com/gl-infra/deployer", ProjectDeployer)
}

func testJobWebhookPipelineName(t *testing.T, project, path, pipelineName string) {
	jobEventMetricsCounter, resetJobEventMetricStub := stubJobEventMetrics(t)
	defer resetJobEventMetricStub()

	jobRetriesCounter, resetJobRetriesMetricStub := stubJobRetriesMetric(t)
	defer resetJobRetriesMetricStub()

	jsonData := webhookRequestJson()
	jsonData["commit"].(map[string]interface{})["name"] = pipelineName

	jobEventMetricsCounter.On("Inc", project, "auto_deploy").Once()
	jobRetriesCounter.On("Inc", project, jsonData["build_name"]).Once()

	assertRequest(t, jsonData, path, project)
}

func assertRequest(t *testing.T, jsonData map[string]interface{}, path, project string) {
	req, err := webhookRequest(jsonData, path, "test1")
	require.NoError(t, err)

	tokenMap := map[string]string{project: "test1"}
	handler := NewJobWebhook(tokenMap).(*jobWebhook)
	rr := testRequest(req, handler)

	require.Equalf(t, http.StatusOK, rr.Code, "handler returned wrong status code: got %v want %v",
		rr.Code, http.StatusOK)
}

func stubJobEventMetrics(t *testing.T) (*mocks.Counter, func()) {
	t.Helper()

	origCounter := jobEventMetric
	resetState := func() { jobEventMetric = origCounter }
	counter := mocks.NewCounter(t)

	jobEventMetric = counter

	return counter, resetState
}

func stubJobRetriesMetric(t *testing.T) (*mocks.Counter, func()) {
	t.Helper()

	origCounter := jobRetriesMetric
	resetState := func() { jobRetriesMetric = origCounter }
	counter := mocks.NewCounter(t)

	jobRetriesMetric = counter

	return counter, resetState
}
