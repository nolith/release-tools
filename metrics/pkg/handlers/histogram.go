package handlers

import (
	"net/http"

	"github.com/gorilla/mux"

	"gitlab.com/gitlab-org/release-tools/metrics/pkg/metrics"
)

type histogram struct {
	metric metrics.Histogram
}

func NewHistogram(metric metrics.Histogram) Pluggable {
	return &histogram{metric}
}

func (c *histogram) PlugRoutes(r *mux.Router) {
	subRouter := r.PathPrefix(route(c.metric)).Subrouter()

	subRouter.Methods("DELETE").HandlerFunc(c.resetHandlerFunc)
	subRouter.HandleFunc("/observe", c.observeHandlerFunc)
}

func (c *histogram) observeHandlerFunc(w http.ResponseWriter, r *http.Request) {
	value, err := getValue(r)
	if err != nil {
		badRequest(w, r, "Missing or wrong value parameter")

		return
	}

	labels := getLabels(r)
	if err := c.metric.CheckLabels(labels); err != nil {
		badRequest(w, r, err.Error())

		return
	}

	c.metric.Observe(value, labels...)

	answer(w, r, "Observed")
}

func (c *histogram) resetHandlerFunc(w http.ResponseWriter, r *http.Request) {
	c.metric.Reset()

	answer(w, r, "Reset")
}
