package handlers

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"net/url"
	"strings"
	"testing"

	"github.com/gorilla/mux"
	"github.com/stretchr/testify/require"

	"gitlab.com/gitlab-org/release-tools/metrics/pkg/metrics"
)

func apiRequest(meta metrics.Metadata, method, action, value, labels string) (*http.Request, error) {
	formData := url.Values{
		"value":  {value},
		"labels": {labels},
	}

	req, err := http.NewRequest(method, route(meta)+"/"+action, strings.NewReader(formData.Encode()))
	if err != nil {
		return nil, err
	}

	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")

	return req, nil
}

func webhookRequestJson() map[string]interface{} {
	return map[string]interface{}{
		"object_kind":           "build",
		"ref":                   "main",
		"tag":                   false,
		"before_sha":            "0000000000000000000000000000000000000000",
		"sha":                   "95d49d1efbd941908580e79d65e4b5ecaf4a8305",
		"retries_count":         2,
		"build_id":              3160521140,
		"build_name":            "auto_deploy:metrics:end_time",
		"build_stage":           "coordinated:finish",
		"build_status":          "created",
		"build_created_at":      "2022-10-12 08:07:05 UTC",
		"build_started_at":      nil,
		"build_finished_at":     "2022-10-12 08:09:29 UTC",
		"build_duration":        nil,
		"build_queued_duration": nil,
		"build_allow_failure":   true,
		"build_failure_reason":  "unknown_failure",
		"pipeline_id":           664563966,
		"runner":                nil,
		"project_id":            31537070,
		"project_name":          "Reuben Pereira / release-tools-fake",
		"user": map[string]interface{}{
			"id":         2967854,
			"name":       "Reuben Pereira",
			"username":   "rpereira2",
			"avatar_url": "https://gitlab.com/uploads/-/system/user/avatar/2967854/avatar.png",
			"email":      "testuser@acme.com",
		},
		"commit": map[string]interface{}{
			"id":           664563966,
			"name":         "auto-deploy: gprd - 15.9.202301250820-cda2eac9bf1.b2fd4b7b8ae",
			"sha":          "95d49d1efbd941908580e79d65e4b5ecaf4a8305",
			"message":      "Remove test jobs and add back other jobs",
			"author_name":  "Reuben Pereira",
			"author_email": "test@acme.com",
			"author_url":   "https://gitlab.com/rpereira2",
			"status":       "canceled",
			"duration":     128,
			"started_at":   "2022-10-12 08:07:06 UTC",
			"finished_at":  "2022-10-12 08:09:29 UTC",
		},
		"repository": map[string]interface{}{
			"name":             "release-tools-fake",
			"url":              "git@gitlab.com:rpereira2/release-tools-fake.git",
			"description":      "",
			"homepage":         "https://ops.gitlab.net/gitlab-org/release/tools",
			"git_http_url":     "https://gitlab.com/rpereira2/release-tools-fake.git",
			"git_ssh_url":      "git@gitlab.com:rpereira2/release-tools-fake.git",
			"visibility_level": 20,
		},
		"environment": nil,
	}
}

func webhookRequest(jsonData map[string]interface{}, project string, token string) (*http.Request, error) {
	marshalledJson, err := json.Marshal(jsonData)
	if err != nil {
		return nil, err
	}

	req, err := http.NewRequest("POST", "/job"+project, bytes.NewReader(marshalledJson))
	if err != nil {
		return nil, err
	}

	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("X-Gitlab-Token", token)
	req.Header.Set("X-Gitlab-Event", "Job Hook")

	return req, nil
}

func testRequest(req *http.Request, h Pluggable) *httptest.ResponseRecorder {
	rr := httptest.NewRecorder()
	handler := mux.NewRouter()

	h.PlugRoutes(handler)
	handler.ServeHTTP(rr, req)

	return rr
}

type mockMetric struct {
	//expectedLabels stores the expected values from the incoming request,
	// each element is the label value we expect to receive during the test
	expectedLabels []string
	value          float64
}

func (m *mockMetric) Namespace() string {
	return "test"
}

func (m *mockMetric) Name() string {
	return "name"
}

func (m *mockMetric) Subsystem() string {
	return "subsystem"
}

func (m *mockMetric) Reset() {
	m.value = 0
}

func (m *mockMetric) apiRequest(action, value string) (*http.Request, error) {
	return apiRequest(m, "POST", action, value, strings.Join(m.expectedLabels, ","))
}

func (m *mockMetric) apiReset() (*http.Request, error) {
	return apiRequest(m, "DELETE", "", "", "")
}

func (m *mockMetric) CheckLabels(labels []string) error {
	if len(labels) != len(m.expectedLabels) {
		return fmt.Errorf("expected %v labels, got %v", len(m.expectedLabels), len(labels))
	}

	for i, label := range labels {
		if label != m.expectedLabels[i] {
			return fmt.Errorf("Label %v - expected %q got %q", i, m.expectedLabels[i], label)
		}
	}

	return nil
}

func (m *mockMetric) testRequest(t *testing.T, req *http.Request, h Pluggable, expectedValue float64) {
	t.Helper()

	rr := testRequest(req, h)

	require.Equal(t, http.StatusOK, rr.Code, "handler returned the wrong status code")
	require.Equal(t, expectedValue, m.value, "unexpected metric value")
}
