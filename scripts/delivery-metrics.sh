#!/bin/sh

revision=${CI_COMMIT_SHORT_SHA:-development}
image="registry.ops.gitlab.net/gitlab-org/release/tools/delivery-metrics:$revision"

case "$1" in
     build)
	 docker build --build-arg "revision=$revision" -t "$image" metrics
	 ;;

     release)
	 if [ -n "${CI+x}" ]; then
	     docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" registry.ops.gitlab.net
	 fi

	 docker tag "$image" registry.ops.gitlab.net/gitlab-org/release/tools/delivery-metrics
	 docker push "$image"
	 docker push registry.ops.gitlab.net/gitlab-org/release/tools/delivery-metrics
	 ;;

     run)
	 PORT=${PORT:-2112}
	 docker run --rm --name delivery_metrics -e JOB_WEBHOOK_RELEASE_TOOLS_OPS_TOKEN="release-tools-token" \
	  -e JOB_WEBHOOK_DEPLOYER_OPS_TOKEN="deployer-token" -e AUTH_TOKEN="acceptance tests" -p "$PORT:$PORT" \
		"$image" -port "$PORT" -log-format color
	 ;;

     acceptance-tests)
	 docker run --rm --link delivery_metrics -e AUTH_TOKEN="acceptance tests" \
		-v "$(pwd):/go/src/gitlab.com/gitlab-org/release-tools/" \
		-w /go/src/gitlab.com/gitlab-org/release-tools/metrics \
		-e CGO_ENABLED=0 \
		golang:1.18-alpine go test -tags=acceptance .
	 ;;

     *)
	 echo "please use build, release, run, or acceptance-tests"
	 exit 1
	 ;;
esac
