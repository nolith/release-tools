# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::PipelineTracer::Job do
  let(:job) do
    build(
      :job,
      name: job_name,
      allow_failure: true,
      started_at: '2022-12-07T22:51:42.229Z',
      finished_at: '2022-12-07T22:52:01.299Z',
      duration: 19.069423,
      queued_duration: 0.60046,
      web_url: 'https://ops.gitlab.net/gitlab-org/release/tools/-/jobs/8694895'
    )
  end

  let(:job_name) { 'job1' }

  describe '#completed?' do
    subject(:job) { described_class.new(job_attributes, ReleaseTools::GitlabOpsClient) }

    let(:job_attributes) { Gitlab::ObjectifiedHash.new(status: status) }

    context 'with non completed status' do
      let(:status) { 'running' }

      it { is_expected.not_to be_completed }
    end

    %w[success failed canceled].each do |status|
      let(:status) { status }

      context "with #{status} status" do
        it { is_expected.to be_completed }
      end
    end
  end

  describe '#root_attributes' do
    subject(:root_attributes) { described_class.new(job, ReleaseTools::GitlabOpsClient).root_attributes }

    let(:job_attributes) do
      {
        job_name: job.name,
        id: job.id,
        web_url: job.web_url,
        allow_failure: job.allow_failure,
        user: job.user.username,
        stage: job.stage,
        started_at: job.started_at,
        finished_at: job.finished_at,
        status: job.status,
        duration: job.duration,
        queued_duration: job.queued_duration
      }.stringify_keys
    end

    it 'returns root attributes' do
      expect(root_attributes).to eq(job_attributes)
    end
  end

  describe '#trace' do
    subject(:trace) { described_class.new(job, ReleaseTools::GitlabOpsClient).trace }

    let(:job_log) { 'correct' }

    before do
      allow(ReleaseTools::GitlabOpsClient).to receive(:job_trace).and_return(job_log)
      allow(ReleaseTools::GitlabDevClient).to receive(:job_trace).and_return(job_log)
      allow(ReleaseTools::GitlabClient).to receive(:job_trace).and_return(job_log)
    end

    it 'calls job_trace API' do
      expect(ReleaseTools::GitlabOpsClient)
        .to receive(:job_trace)
        .with(job.pipeline.project_id, job.id)

      expect(trace).to eq(job_log)
    end
  end

  describe '#triggered_pipeline_url' do
    subject(:triggered_pipeline_url) { described_class.new(job, ReleaseTools::GitlabOpsClient).triggered_pipeline_url }

    let(:job_log) { instance_double(String, match: nil) }
    let(:job_name) { 'wait:cng' }

    before do
      allow(ReleaseTools::GitlabOpsClient).to receive(:job_trace).and_return(job_log)
    end

    it 'memoizes nil return value' do
      expect(job_log).to receive(:match).exactly(4).times

      instance = described_class.new(job, ReleaseTools::GitlabOpsClient)

      instance.triggered_pipeline_url
      expect(instance.triggered_pipeline_url).to be_nil
    end

    it 'memoizes non-nil return value' do
      allow(job_log).to receive(:match).and_return({ pipeline_url: 'url' })

      expect(job_log).to receive(:match).once

      instance = described_class.new(job, ReleaseTools::GitlabOpsClient)

      instance.triggered_pipeline_url
      expect(instance.triggered_pipeline_url).to eq('url')
    end

    context 'with different job name' do
      let(:job_name) { 'job1' }

      it 'ignores jobs whose names are not in trigger jobs list' do
        expect(ReleaseTools::GitlabOpsClient).not_to receive(:job_trace)

        expect(triggered_pipeline_url).to be_nil
      end
    end

    context 'with log message from wait:cng' do
      let(:job_log) { 'Checking status of pipeline -- {:pipeline=>"https://ops.gitlab.net/gitlab-com/gl-infra/deployer/-/pipelines/548997"}' }

      it 'returns pipeline URL' do
        expect(triggered_pipeline_url).to eq('https://ops.gitlab.net/gitlab-com/gl-infra/deployer/-/pipelines/548997')
      end
    end

    context 'with log message from DeployTrigger class' do
      let(:job_log) { 'Triggered deployer pipeline -- {:url=>"https://ops.gitlab.net/gitlab-com/gl-infra/deployer/-/pipelines/548997"}' }

      it 'returns pipeline URL' do
        expect(triggered_pipeline_url).to eq('https://ops.gitlab.net/gitlab-com/gl-infra/deployer/-/pipelines/548997')
      end
    end

    context 'with log message from Deployer triggered QA jobs' do
      let(:job_log) { 'Triggered downstream pipeline -- {:web_url=>"https://ops.gitlab.net/gitlab-org/gitlab-qa/-/pipelines/999757"}' }

      it 'returns pipeline URL' do
        expect(triggered_pipeline_url).to eq('https://ops.gitlab.net/gitlab-org/gitlab-qa/-/pipelines/999757')
      end
    end

    context 'with log message from Deployer kubernetes jobs' do
      let(:job_log) { '  msg: Checking pipeline https://ops.gitlab.net/gitlab-com/gl-infra/k8s-workloads/gitlab-com/-/pipelines/1671026 for a completed status' }

      it 'returns pipeline URL' do
        expect(triggered_pipeline_url).to eq('https://ops.gitlab.net/gitlab-com/gl-infra/k8s-workloads/gitlab-com/-/pipelines/1671026')
      end
    end
  end

  describe '#triggered_downstream_pipeline?' do
    subject(:instance) { described_class.new(job, ReleaseTools::GitlabOpsClient) }

    let(:triggered_url) { nil }
    let(:job_name) { 'wait:cng' }

    before do
      instance.instance_variable_set(:@triggered_pipeline_url, triggered_url)
    end

    context 'when triggered_pipeline_url is nil' do
      let(:triggered_url) { nil }

      it 'returns false' do
        expect(instance.triggered_downstream_pipeline?).to be(false)
      end
    end

    context 'when triggered_pipeline_url is not nil' do
      let(:triggered_url) { 'url' }

      it 'returns false' do
        expect(instance.triggered_downstream_pipeline?).to be(true)
      end
    end
  end
end
