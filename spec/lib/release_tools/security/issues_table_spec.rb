# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::Security::IssuesTable do
  subject(:generate) { described_class.new(client).generate }

  let(:mr1) do
    build(
      :merge_request,
      id: 1,
      project_id: 1
    )
  end

  let(:release_issue) do
    build(:issue, id: 1, project_id: 1)
  end

  let(:issues_crawler) do
    instance_double(
      ReleaseTools::Security::IssueCrawler,
      upcoming_security_issues_and_merge_requests: issues,
      release_issue: release_issue
    )
  end

  let(:issue1) do
    build(
      :issue,
      id: 1,
      project_id: 1,
      pending_reason: nil,
      processed?: false,
      ready_to_be_processed?: nil,
      default_merge_request_merged?: false,
      mwps_set_on_default_merge_request?: true,
      default_merge_request_deployed?: false,
      backports_merged?: false,
      merge_request_targeting_default_branch: mr1
    )
  end

  let(:issues) { [issue1] }
  let(:client) { instance_spy(ReleaseTools::Security::Client) }

  before do
    allow(ReleaseTools::Security::IssueCrawler)
      .to receive(:new)
      .and_return(issues_crawler)

    allow(ReleaseTools::GitlabClient).to receive(:issue_notes).and_return(double(auto_paginate: []))
  end

  describe '#generate' do
    context 'when default branch MR has been merged' do
      let(:issue1) do
        build(
          :issue,
          id: 1,
          project_id: 1,
          pending_reason: nil,
          processed?: false,
          ready_to_be_processed?: nil,
          default_merge_request_merged?: true,
          mwps_set_on_default_merge_request?: true,
          default_merge_request_deployed?: false,
          backports_merged?: false,
          merge_request_targeting_default_branch: mr1
        )
      end

      it 'adds a checkmark' do
        expect(ReleaseTools::GitlabClient)
          .to receive(:create_issue_note)
          .with(
            release_issue.project_id,
            hash_including(body: include("| #{issue1.web_url} | :white_check_mark: |"))
          )

        without_dry_run { generate }
      end
    end

    context 'when backports have been merged' do
      let(:issue1) do
        build(
          :issue,
          iid: 1,
          project_id: 1,
          pending_reason: nil,
          processed?: false,
          ready_to_be_processed?: nil,
          default_merge_request_merged?: false,
          mwps_set_on_default_merge_request?: true,
          default_merge_request_deployed?: false,
          backports_merged?: true,
          merge_request_targeting_default_branch: mr1
        )
      end

      it 'adds a checkmark' do
        expect(ReleaseTools::GitlabClient)
          .to receive(:create_issue_note)
          .with(
            release_issue.project_id,
            hash_including(body: include("| #{issue1.web_url} |  |  | :white_check_mark: |"))
          )

        without_dry_run { generate }
      end
    end

    context 'when default MR has been deployed' do
      let(:issue1) do
        build(
          :issue,
          iid: 1,
          project_id: 1,
          pending_reason: nil,
          processed?: false,
          ready_to_be_processed?: nil,
          default_merge_request_merged?: false,
          mwps_set_on_default_merge_request?: true,
          default_merge_request_deployed?: true,
          backports_merged?: false,
          merge_request_targeting_default_branch: mr1
        )
      end

      it 'adds a checkmark' do
        expect(ReleaseTools::GitlabClient)
          .to receive(:create_issue_note)
          .with(
            release_issue.project_id,
            hash_including(body: include("| #{issue1.web_url} |  | :white_check_mark: |"))
          )

        without_dry_run { generate }
      end
    end

    context 'when there is a pending_reason' do
      let(:issue1) do
        build(
          :issue,
          iid: 1,
          project_id: 1,
          pending_reason: 'reason',
          processed?: false,
          ready_to_be_processed?: false,
          default_merge_request_merged?: false,
          mwps_set_on_default_merge_request?: true,
          default_merge_request_deployed?: false,
          backports_merged?: false,
          merge_request_targeting_default_branch: mr1
        )
      end

      it 'adds a comment' do
        expect(ReleaseTools::GitlabClient)
          .to receive(:create_issue_note)
          .with(
            release_issue.project_id,
            hash_including(body: include("| #{issue1.web_url} |  |  |  | reason"))
          )

        without_dry_run { generate }
      end

      context 'when issue has already been processed' do
        let(:issue1) do
          build(
            :issue,
            iid: 1,
            project_id: 1,
            pending_reason: 'reason',
            processed?: true,
            ready_to_be_processed?: false,
            default_merge_request_merged?: false,
            mwps_set_on_default_merge_request?: false,
            default_merge_request_deployed?: false,
            backports_merged?: false,
            merge_request_targeting_default_branch: mr1
          )
        end

        it 'does not add comment' do
          expect(ReleaseTools::GitlabClient)
            .to receive(:create_issue_note)
            .with(
              release_issue.project_id,
              hash_including(body: include("| #{issue1.web_url} |  |  |  |  |"))
            )

          without_dry_run { generate }
        end
      end
    end

    context 'when MWPS has been set on default MR' do
      let(:issue1) do
        build(
          :issue,
          iid: 1,
          project_id: 1,
          pending_reason: nil,
          processed?: false,
          ready_to_be_processed?: true,
          default_merge_request_merged?: false,
          mwps_set_on_default_merge_request?: true,
          default_merge_request_deployed?: false,
          backports_merged?: false,
          merge_request_targeting_default_branch: mr1
        )
      end

      it 'adds a comment' do
        expect(ReleaseTools::GitlabClient)
            .to receive(:create_issue_note)
            .with(
              release_issue.project_id,
              hash_including(body: include(
                "| #{issue1.web_url} |  |  |  | MWPS set on default branch MR: https://example.com/foo/bar/-/merge_requests/1"
              ))
            )

        without_dry_run { generate }
      end
    end

    context 'when table exists' do
      let(:mr2) do
        build(
          :merge_request,
          id: 2,
          project_id: 1
        )
      end

      let(:issue1) do
        build(
          :issue,
          id: 1,
          project_id: 1,
          pending_reason: nil,
          processed?: false,
          ready_to_be_processed?: nil,
          default_merge_request_merged?: false,
          mwps_set_on_default_merge_request?: true,
          default_merge_request_deployed?: false,
          backports_merged?: false,
          merge_request_targeting_default_branch: mr1
        )
      end

      let(:issue2) do
        build(
          :issue,
          id: 2,
          project_id: 1,
          pending_reason: nil,
          processed?: false,
          ready_to_be_processed?: nil,
          default_merge_request_merged?: true,
          mwps_set_on_default_merge_request?: true,
          default_merge_request_deployed?: false,
          backports_merged?: false,
          merge_request_targeting_default_branch: mr2
        )
      end

      let(:existing_note_body) do
        <<~EOF
          ## Security issues

          | Issue                          | Master merged?     | Deployed? | Backports merged? | Bot Comments | Release manager comments |
          | ------------------------------ | ------------------ | ----------| ----------------- | ------------ | ------------------------ |
          | #{issue1.web_url} | | | | | Some comment   |
          | #{issue2.web_url}  | :white_check_mark:  |   | ||   Some other comment

          ---

          :robot: <sub>This table was generated by [release-tools](https://gitlab.com/gitlab-org/release-tools/).
          Please open an issue in the [Delivery team issue tracker](https://gitlab.com/gitlab-com/gl-infra/delivery/-/issues)
          if you have any suggestions or bug reports.</sub>
        EOF
      end

      let(:issues) { [issue1, issue2] }

      before do
        allow(ReleaseTools::GitlabClient)
          .to receive(:issue_notes)
          .and_return(double(auto_paginate: [
            build(
              :note,
              id: 1,
              noteable_id: release_issue.id,
              body: existing_note_body,
              author: double(username: ReleaseTools::Security::Client::RELEASE_TOOLS_BOT_USERNAME)
            )
          ]))
      end

      it 'parses and retains release manager comments' do
        expect(ReleaseTools::GitlabClient)
            .to receive(:edit_issue_note)
            .with(
              release_issue.project_id,
              hash_including(
                body:
                  <<~EOF
                    ## Security issues

                    | Issue | Master merged? | Deployed? | Backports merged? | Bot Comments | Release manager comments |
                    |-------|----------------|-----------|-------------------|--------------|--------------------------|
                    | #{issue2.web_url} | :white_check_mark: |  |  |  | Some other comment |
                    | #{issue1.web_url} |  |  |  |  | Some comment |

                    ---

                    :robot: <sub>This table was generated by [release-tools](https://gitlab.com/gitlab-org/release-tools/).
                    Please open an issue in the [Delivery team issue tracker](https://gitlab.com/gitlab-com/gl-infra/delivery/-/issues)
                    if you have any suggestions or bug reports.</sub>
                  EOF
              )
            )

        without_dry_run { generate }
      end
    end

    context 'with dry run' do
      it 'does not post comment' do
        expect(ReleaseTools::GitlabClient)
          .not_to receive(:create_issue_note)

        generate
      end
    end
  end
end
