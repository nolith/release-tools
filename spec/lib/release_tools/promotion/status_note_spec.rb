# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::Promotion::StatusNote do
  let(:version) { '42.0.202005220540-7c84ccdc806.59f00bb0515' }
  let(:ci_pipeline_url) { 'https://example.com/pipeline' }
  let(:status) { double('status', fine?: true, to_issue_body: 'a collection of check results') }
  let(:override_reason) { 'false' }
  let(:services_status) { [] }
  let(:client) { spy('ReleaseTools::GitlabOpsClient') }
  let(:definitions) { double('Definitions') }

  let(:user) do
    build(:user, username: 'release-manager-user', production: 'release-manager-user')
  end

  let(:pipeline_jobs) do
    [
      build(:job, name: 'promote', user: user),
      build(:job, name: 'deploy:gstg'),
      build(:job, name: 'deploy:gprd')
    ]
  end

  subject(:note) do
    described_class.new(
      status: status,
      package_version: version,
      override_reason: override_reason
    )
  end

  before do
    stub_const('ReleaseTools::GitlabOpsClient', client)

    allow(client)
      .to receive(:pipeline_jobs)
      .and_return(pipeline_jobs)
  end

  describe '#release_manager' do
    before do
      allow(ReleaseTools::ReleaseManagers::Definitions)
        .to receive(:new)
        .and_return(definitions)
    end

    context 'when fetching from the coordinated pipeline' do
      it 'returns release manager that triggered the promote job' do
        expect(definitions).to receive(:find_user)
          .with('release-manager-user', instance: :ops)
          .and_return(user)

        ClimateControl.modify(RELEASE_MANAGER: 'foo') do
          expect(note.release_manager).to eq('@release-manager-user')
        end
      end
    end

    context 'when not a coordinated pipeline' do
      let(:pipeline_jobs) { [build(:job, name: 'auto_deploy:check_production')] }

      it 'reads the username from RELEASE_MANAGER env variable' do
        release_manager = 'username'

        expect(definitions).to receive(:find_user)
          .with(release_manager, instance: :ops)
          .and_return(build(:user, production: 'another-username'))

        ClimateControl.modify(RELEASE_MANAGER: release_manager, IGNORE_PRODUCTION_CHECKS: 'foo') do
          expect(note.release_manager).to eq('@another-username')
        end
      end

      it 'tracks unknown release managers' do
        ClimateControl.modify(RELEASE_MANAGER: 'ops-unknown', IGNORE_PRODUCTION_CHECKS: 'foo') do
          expect(definitions).to receive(:find_user)
            .with('ops-unknown', instance: :ops)

          expect(note.release_manager)
            .to eq(":warning: Unknown release manager! OPS username ops-unknown")
        end
      end
    end
  end

  describe '#body' do
    let(:definitions) { double('Definitions') }
    let(:user) { build(:user, username: 'liz.lemon') }

    before do
      allow(ReleaseTools::ReleaseManagers::Definitions)
        .to receive(:new)
        .and_return(definitions)

      allow(definitions)
        .to receive(:find_user)
        .and_return(double('User', production: 'liz.lemon'))
    end

    around do |ex|
      ClimateControl.modify(CI_PIPELINE_URL: ci_pipeline_url, CI_PIPELINE_ID: '123', &ex)
    end

    it 'links the pipeline' do
      expect(note.body).to include("[deployer pipeline](#{ci_pipeline_url})")
    end

    it 'includes the generated report' do
      expect(note.body).to include(status.to_issue_body)
    end

    context 'when the pipeline url is nil' do
      let(:ci_pipeline_url) { nil }

      it 'does not link the pipeline' do
        expect(note.body).not_to include('deployer pipeline')
      end
    end

    it 'pings the release manager based on the coordinated pipeline jobs' do
      expect(definitions).to receive(:find_user)
        .with('liz.lemon', instance: :ops)

      status_note = note.body

      expect(status_note).to include("@liz.lemon")
    end

    it 'includes the package version' do
      expect(note.body).to include("started a production deployment for package `#{version}`")
    end

    it "doesn't include the overriding reason when not override_status is false" do
      expect(note.body).not_to include(override_reason)
    end

    context 'when overriding the status' do
      let(:override_reason) { 'a reason to override the status' }

      it 'warns the status was overridden' do
        expect(note.body).to include(":warning: Status check overridden!")
        expect(note.body).to include(override_reason)
      end
    end
  end
end
