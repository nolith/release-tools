# frozen_string_literal: true

namespace :quality do
  desc 'Sends a Slack notification about the status of qa'
  task :notify do
    ReleaseTools::AutoDeploy::CoordinatedPipeline::Qa::Notifier.new(
      pipeline_id: ENV.fetch('CI_PIPELINE_ID', nil),
      deploy_version: ENV.fetch('DEPLOY_VERSION', nil),
      environment: ENV.fetch('DEPLOY_ENVIRONMENT', nil)
    ).execute
  end
end
