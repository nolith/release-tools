# frozen_string_literal: true

module ReleaseTools
  module Slack
    class Message
      include ::SemanticLogger::Loggable

      API_URL = 'https://slack.com/api/chat.postMessage'

      NoCredentialsError = Class.new(StandardError)
      CouldNotPostMessageError = Class.new(StandardError)

      def self.post(channel:, message:, blocks: [], mrkdwn: nil, additional_options: {})
        raise NoCredentialsError unless slack_token.present?

        additional_options[:unfurl_links] = false if additional_options[:unfurl_links].nil?
        additional_options[:unfurl_media] = false if additional_options[:unfurl_media].nil?

        params = {
          channel: channel,
          text: message,
          username: Slack::CHATOPS_USERNAME,
          icon_url: Slack::CHATOPS_ICON_URL,
          mrkdwn: mrkdwn
        }
        .merge(additional_options)
        .compact

        params[:blocks] = blocks if blocks.any?

        logger.trace(__method__, params)

        return {} if SharedStatus.dry_run?

        if Feature.enabled?(:slack_down)
          logger.warn("Not attempting to call Slack API because FF 'slack_down' is enabled", params: params)
          return {}
        end

        response = client.post(
          API_URL,
          json: params
        )

        unless response.status.success?
          logger.warn('Slack API call was not successful', code: response.code, response_body: response.body, request_params: params)
          raise CouldNotPostMessageError.new("#{response.code} #{response.reason} #{response.body}")
        end

        body = JSON.parse(response.body)
        return body if body['ok']

        logger.warn('Slack API call returned error', response_body: body, request_params: params)
        raise CouldNotPostMessageError.new("#{response.code} #{response.reason} #{body}")
      end

      def self.client
        @client ||= HTTP.auth("Bearer #{slack_token}")
      end

      def self.slack_token
        ENV.fetch('SLACK_APP_BOT_TOKEN', '')
      end
    end
  end
end
