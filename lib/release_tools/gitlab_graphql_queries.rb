# frozen_string_literal: true

require 'release_tools/graphql_adapter'

module ReleaseTools
  class GitlabGraphqlQueries
    MERGE_TRAINS_ENABLED_QUERY = ReleaseTools::GitlabClient.graphql_client.parse <<~QUERY
      query($project: ID!) {
        project(fullPath: $project) {
          id
          ciCdSettings {
            mergeTrainsEnabled
          }
        }
      }
    QUERY

    ADD_TO_MERGE_TRAIN_QUERY = ReleaseTools::GitlabClient.graphql_client.parse <<~QUERY
      mutation($project: ID!, $mr: String!, $sha: String!) {
        mergeRequestAccept(input: { projectPath: $project, iid: $mr, strategy: MERGE_TRAIN, sha: $sha, shouldRemoveSourceBranch: true }) {
          errors
          mergeRequest {
            state
            mergeable
            autoMergeEnabled
            autoMergeStrategy
            inProgressMergeCommitSha
            mergeOngoing
            mergeTrainsCount
            shouldRemoveSourceBranch
            mergeUser {
              id
              username
            }
          }
        }
      }
    QUERY
  end
end
