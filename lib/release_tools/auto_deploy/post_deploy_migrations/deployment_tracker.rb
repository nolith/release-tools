# frozen_string_literal: true

module ReleaseTools
  module AutoDeploy
    module PostDeployMigrations
      class DeploymentTracker
        include ::SemanticLogger::Loggable
        include ReleaseTools::Tracker::Deployment

        # The environments that we support
        ENVIRONMENTS = %w[db/gstg db/gprd].freeze

        # environment - The name of the post-deploy environment that was deployed to.
        #   Options can be db/gstg or db/gprd.
        # status - The status of the deployment: 'running', 'success' or 'failed'
        # version - The auto-deploy package
        def initialize(environment:, status:, version:)
          @environment = environment
          @status = status
          @version = version
        end

        private

        attr_reader :environment, :status, :version

        def perform_checks
          check_status
          check_environment
        end

        def check_environment
          return if ENVIRONMENTS.include?(environment)

          raise ArgumentError, "The environment #{environment} is not supported"
        end

        def versions
          gitlab_version = product_version['gitlab-ee']

          logger.info(
            "Current auto_deploy package deployed to #{environment}",
            version: version,
            sha: gitlab_version.sha,
            ref: gitlab_version.ref,
            is_tag: gitlab_version.tag?
          )

          [
            [Project::GitlabEe, gitlab_version]
          ]
        end
      end
    end
  end
end
