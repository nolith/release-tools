# frozen_string_literal: true

require 'sentry-raven'

module ReleaseTools
  module Promotion
    class StatusNote
      include ::SemanticLogger::Loggable
      include ReleaseTools::AutoDeploy::Pipeline

      attr_reader :status, :package_version, :override_reason

      def initialize(status:, package_version:, override_reason:)
        @status = status
        @package_version = package_version
        @ci_pipeline_url = ci_pipeline_url
        @override_reason = override_reason
      end

      def override_status?
        override_reason != 'false'
      end

      def ci_pipeline_url
        ENV.fetch('DEPLOYER_PIPELINE_URL', ENV.fetch('CI_PIPELINE_URL', nil))
      end

      def release_manager
        ops_user = find_release_manager

        return ':warning: Unknown release manager!' if ops_user.nil?

        user = ReleaseManagers::Definitions.new.find_user(ops_user, instance: :ops)

        return ":warning: Unknown release manager! OPS username #{ops_user}" if user.nil?

        "@#{user.production}"
      end

      def body
        ERB
          .new(template, trim_mode: '-') # Omit blank lines when using `<% -%>`
          .result(binding)
      end

      private

      def find_release_manager
        logger.info('Fetching release manager', pipeline_url: pipeline_id, package_version: package_version, env_release_manager: ENV.fetch('RELEASE_MANAGER', nil))

        release_manager_from_coordinated_pipeline || ENV.fetch('RELEASE_MANAGER')
      end

      def release_manager_from_coordinated_pipeline
        coordinated_pipeline_jobs
          .find { |job| job.name == 'promote' }
          &.user
          &.username
      end

      def coordinated_pipeline_jobs
        Retriable.with_context(:api) do
          logger.debug('Finding coordinated pipeline_jobs', pipeline_id: pipeline_id)

          ReleaseTools::GitlabOpsClient.pipeline_jobs(Project::ReleaseTools, pipeline_id)
        end
      end

      def pipeline_id
        ENV.fetch('CI_PIPELINE_ID', nil)
      end

      def template
        template_path = File.expand_path('../../../templates/status_note.md.erb', __dir__)

        File.read(template_path)
      end
    end
  end
end
