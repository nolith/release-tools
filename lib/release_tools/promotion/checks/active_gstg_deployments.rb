# frozen_string_literal: true

module ReleaseTools
  module Promotion
    module Checks
      class ActiveGstgDeployments
        include ::ReleaseTools::Promotion::Check
        include ActiveDeployments

        def env
          ::ReleaseTools::PublicRelease::Release::STAGING_ENVIRONMENT
        end
      end
    end
  end
end
