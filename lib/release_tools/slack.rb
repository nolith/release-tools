# frozen_string_literal: true

require 'slack-ruby-block-kit'

require 'release_tools/slack/webhook'
require 'release_tools/slack/utilities'
require 'release_tools/slack/auto_deploy_notification'
require 'release_tools/slack/coordinated_pipeline_diffs_notification'
require 'release_tools/slack/coordinated_pipeline_notification'
require 'release_tools/slack/coordinated_pipeline_tag_notification'
require 'release_tools/slack/chatops_notification'
require 'release_tools/slack/merge_train_notification'
require 'release_tools/slack/message'
require 'release_tools/slack/post_deploy_migrations_notification'
require 'release_tools/slack/post_deploy_pipeline_initial_notification'
require 'release_tools/slack/qa_notification'
require 'release_tools/slack/search'
require 'release_tools/slack/tag_notification'

module ReleaseTools
  module Slack
    ANNOUNCEMENTS = 'C8PKBH3M5'
    DEPLOYMENT_NOTIFICATION_TESTS = 'C01FTC3AE85'
    F_UPCOMING_RELEASE = 'C0139MAV672'
    GITALY_ALERTS = 'C4MU5R2MD'
    RELEASE_MANAGERS = 'S0127FU8PDE'
    TEAM = 'T02592416'

    ANNOUNCEMENTS_NAME = 'announcements'

    CHATOPS_USERNAME = 'GitLab-Chatops'
    CHATOPS_ICON_URL = 'https://avatars.slack-edge.com/2018-05-01/355645466080_4202bbc846089f270a4e_512.png'
  end
end
